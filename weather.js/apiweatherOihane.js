
fetch("https://api.open-meteo.com/v1/forecast?latitude=37.99&longitude=-1.13&hourly=temperature_2m&daily=temperature_2m_max,temperature_2m_min,weathercode,precipitation_sum&timezone=Europe/Madrid")
 .then(function (respuesta){return respuesta.json()}) 
 .then(procesar_datos); 
 
/* falta añadir unidades y fecha*/

function procesar_datos (contenido) {
  let temp_max = contenido.daily.temperature_2m_max;
  let temp_min = contenido.daily.temperature_2m_min;
  let precipitation_sum= contenido.daily.precipitation_sum;
  selector(temp_max, temp_min, precipitation_sum);
}

function selector (temp_max, temp_min, precipitation_sum) {
  let selectortemp_max = document.querySelectorAll (".max");
  let selectortemp_min = document.querySelectorAll (".min");
  let selectorprecipitation_sum = document.querySelectorAll (".prec");

  for(let i=0; i<selectortemp_max.length; i++){
    selectortemp_max[i].innerHTML= "Temperatura máxima: " + temp_max[i];
  }

  for (let i=0; i<selectortemp_min.length; i++){
    selectortemp_min[i].innerHTML= "Temperatura mínima: " + temp_min [i];
  }

 for (let i=0; i<selectorprecipitation_sum.length; i++) {
  selectorprecipitation_sum[i].innerHTML= "Precipitaciones: " + precipitation_sum[i];
}
}

